# Update the Gemfile
gem "ocean-rails"

# Run bundle install - we need the generators in it now
run "bundle install"

# Create a temporary config.yml file
file 'config/config.yml', <<-CODE
# This is a temporary file which will be overwritten during setup
BASE_DOMAIN: example.com
CODE

# Set up the application as a SOA service Rails application
generate "ocean_setup", app_name

# Install the required gems and package them with the app
run "bundle install"

# Remove the asset stuff from the application conf file
gsub_file "config/application.rb",
          /    # Enable the asset pipeline.+config\.assets\.version = '1\.0'\s/m, ''

# Get the DBs in order
rake "db:migrate"
