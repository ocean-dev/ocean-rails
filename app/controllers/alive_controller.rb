class AliveController < ApplicationController

  skip_before_action :require_x_api_token
  skip_before_action :authorize_action


  def index
    render plain: "ALIVE", status: 200
  end

end
