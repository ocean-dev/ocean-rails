f = File.join(Rails.root, "config/config.yml")

# If there is a file to process, do so
if f
  cfg = YAML.load(ERB.new(File.read(f)).result)
  cfg.merge! cfg.fetch(Rails.env, {}) if cfg.fetch(Rails.env, {})
  cfg.each do |k, v|
    next if k =~ /[a-z]/
    override = ENV["OVERRIDE_#{k}"]
    if override && override != ""
      master, staging = override.split(',')
      if staging.present? && k == "API_PASSWORD"
        pw = (ENV['GIT_BRANCH'] == 'staging' ? staging : master)
        eval "#{k} = #{pw.inspect}"
      else
        eval "#{k} = #{override.inspect}"
      end
    else
      eval "#{k} = #{v.inspect}"
    end
  end
else
  # Otherwise print an error message and abort.
  puts
  puts "-----------------------------------------------------------------------"
  puts "Constant definition file missing."
  puts "-----------------------------------------------------------------------"
  puts
  abort
end
