# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :the_model do
    name         { "the_model_#{rand(1000000)}" }
    description  { "This is a description of the_model." }
    vip          { "Greetings, O superuser!" }
    created_by   { "https://forbidden.example.com/v1/api_users/a-b-c-d-e" }
    updated_by   { "https://forbidden.example.com/v1/api_users/e-d-c-b-a" }
    lock_version { 0 }
  end
end
