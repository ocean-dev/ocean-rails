require 'spec_helper'

describe "common code", :type => :helper do

  describe "hyperlinks" do

    it "should return an empty hash given no args" do
  	  expect(hyperlinks()).to eq({})
    end

    it "should return as many output array elements as input hash args" do
      expect(hyperlinks(self: "http://foo",
      	         quux: "https://blah").count).to eq 2
    end

    it "should return a two-element hash for each arg" do
      expect(hyperlinks(self: "https://example.com/v1/blah")['self'].count).to eq 2
    end

    it "should return a href value for the value of each arg" do
      expect(hyperlinks(self: "blah")['self']['href']).to eq "blah"
    end

    it "should default the type to application/json for terse hyperlinks" do
      expect(hyperlinks(self: "blah")['self']['type']).to eq "application/json"
    end

    it "should accept non-terse values giving the href and type in a sub-hash" do
      hl = hyperlinks(self: {href: "https://xux", type: "image/jpeg"})
      expect(hl['self']['href']).to eq "https://xux"
      expect(hl['self']['type']).to eq "image/jpeg"
    end
  end


  describe "smart_api_user_url" do

  	it "should accept exactly one argument" do
      expect { smart_api_user_url() }.to raise_error(ArgumentError)
      expect { smart_api_user_url(1, 2) }.to raise_error(ArgumentError)
  	end

    it "should build an ApiUser URI when given an integer" do
      expect(smart_api_user_url(123)).to eq "http://dev-api.example.com/v1/api_users/123"
    end

  	it "should accept a nil or false argument and return false" do
      expect(smart_api_user_url(nil)).to   eq false
      expect(smart_api_user_url(false)).to eq false
  	end

    it "should accept a blank string and return false" do
      expect(smart_api_user_url("")).to  eq false
      expect(smart_api_user_url(" ")).to eq false
    end

    it "should return a non-empty string unchanged if it is a complete URI" do
      uri = "http://dev-api.example.com/v1/api_users/1-2-3-4-5"
      expect(smart_api_user_url(uri)).to eq uri
      uri = "http://dev-api.example.com/v1/api_users/1-2-3-4-5"
      expect(smart_api_user_url(uri)).to eq uri
    end

    it "should turn a bare UUID string into a complete URI" do
      expect(smart_api_user_url("aa-bb-cc-dd-ee")).
        to eq "http://dev-api.example.com/v1/api_users/aa-bb-cc-dd-ee"
    end

    it "should raise an error if not given an integer, string, or nil" do
      expect { smart_api_user_url(:wrong) }.to raise_error(RuntimeError)
    end
  end


  # describe "aws_config" do
  #
  #   it "should let access keys override the others" do
  #     allow(ENV).to receive(:[]).with("AWS_ACCESS_KEY_ID").and_return("axesz")
  #     allow(ENV).to receive(:[]).with("AWS_SECRET_ACCESS_KEY").and_return("secaxesz")
  #     allow(ENV).to receive(:[]).with("SOME_ENDPOINT").and_return("http://here:12345")
  #     allow(ENV).to receive(:[]).with("AWS_REGION").and_return("eu-west-1")
  #     expect(aws_config(endpoint: 'SOME_ENDPOINT')).
  #       to match a_hash_including(
  #         region: "eu-west-1",
  #         credentials: an_instance_of(Aws::Credentials)
  #       )
  #   end
  #
  #   it "should let an explicit endpoint override instance profile credentials" do
  #     allow(ENV).to receive(:[]).with("AWS_ACCESS_KEY_ID").and_return(nil)
  #     allow(ENV).to receive(:[]).with("AWS_SECRET_ACCESS_KEY").and_return(nil)
  #     allow(ENV).to receive(:[]).with("SOME_ENDPOINT").and_return("http://here:12345")
  #     allow(ENV).to receive(:[]).with("AWS_REGION").and_return("eu-east-1")
  #     expect(aws_config(endpoint: 'SOME_ENDPOINT')).to match a_hash_including(
  #       region: "eu-west-1",
  #       credentials: an_instance_of(Aws::Credentials),
  #       endpoint: "http://here:12345"
  #     )
  #   end
  #
  #   it "should let an explicit endpoint override instance profile credentials" do
  #     allow(ENV).to receive(:[]).with("AWS_ACCESS_KEY_ID").and_return(nil)
  #     allow(ENV).to receive(:[]).with("AWS_SECRET_ACCESS_KEY").and_return(nil)
  #     allow(ENV).to receive(:[]).with("SOME_ENDPOINT").and_return("http://here:12345")
  #     allow(ENV).to receive(:[]).with("AWS_REGION").and_return("eu-east-1")
  #     expect(aws_config(endpoint: 'SOME_ENDPOINT')).to match a_hash_including(
  #       region: "eu-west-1",
  #       credentials: an_instance_of(Aws::Credentials),
  #       endpoint: "http://here:12345"
  #     )
  #   end
  #
  #   it "should ignore an explicit endpoint if the corresponding ENV var is missing" do
  #     allow(ENV).to receive(:[]).with("AWS_ACCESS_KEY_ID").and_return(nil)
  #     allow(ENV).to receive(:[]).with("AWS_SECRET_ACCESS_KEY").and_return(nil)
  #     allow(ENV).to receive(:[]).with("SOME_ENDPOINT").and_return(nil)
  #     allow(ENV).to receive(:[]).with("AWS_REGION").and_return("eu-east-1")
  #     expect(aws_config(endpoint: 'SOME_ENDPOINT')).to match a_hash_including(
  #       region: "eu-east-1",
  #       credentials: an_instance_of(Aws::InstanceProfileCredentials)
  #     )
  #   end
  #
  # end

end
