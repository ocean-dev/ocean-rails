require 'spec_helper'

describe Api, :type => :request do

  describe ".ban" do

    before :each do
      stub_const("VARNISH_CACHES", ["local.varnish.host"])
      stub_request(:ban, "http://local.varnish.host/a-b-c-d-e").
        to_return(status: 200, body: "", headers: {})
    end

    it "should not add another / between the host and path if there's one already" do
      Api.ban "/a-b-c-d-e"
    end

    it "should add a / between the host and path if there's none" do
      Api.ban "a-b-c-d-e"
    end

  end

end
