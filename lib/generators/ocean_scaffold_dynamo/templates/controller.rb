<% if namespaced? -%>
require_dependency "<%= namespaced_file_path %>/application_controller"

<% end -%>
<% module_namespacing do -%>
class <%= controller_class_name %>Controller < ApplicationController

  ocean_resource_controller

  before_action :find_<%= singular_table_name %>, :only => [:show, :update, :destroy]
  before_action :verify_attributes, only: :update


  # GET <%= route_url %>
  def index
    expires_in 0, 's-maxage' => DEFAULT_CACHE_TIME
    # if stale?(collection_etag(<%= class_name %>))   # collection_etag is still ActiveRecord only!
    # Instead, we get all the instances and compute the ETag manually.
    # You may wish to remove this action entirely. You may also wish to remove the caching,
    # in which case you don't need to compute the Etag at all.
    @<%= plural_table_name %> = <%= class_name %>.all
    latest = @<%= plural_table_name %>.max_by(&:updated_at)
    last_updated = latest && latest.updated_at
    if stale?(etag: "<%= class_name %>:#{@<%= plural_table_name %>.length}:#{last_updated}")
      api_render @<%= plural_table_name %>
    end
  end


  # GET <%= route_url %>/a-b-c-d-e
  def show
    expires_in 0, 's-maxage' => DEFAULT_CACHE_TIME
    if stale?(etag: @<%= singular_table_name %>.lock_version,          # NB: DynamoDB tables don't have cache_key - FIX!
              last_modified: @<%= singular_table_name %>.updated_at)
      api_render @<%= singular_table_name %>
    end
  end


  # POST <%= route_url %>
  def create
    ActionController::Parameters.permit_all_parameters = true
    name = params[:name]
    description = params[:description]
    @<%= singular_table_name %> = <%= class_name %>.new(name: name, description: description)
    set_updater(@<%= singular_table_name %>)
    @<%= singular_table_name %>.save!
    api_render @<%= singular_table_name %>, new: true
  end


  # PUT <%= route_url %>/a-b-c-d-e
  def update
    @<%= singular_table_name %>.name = params[:name] if params[:name]
    @<%= singular_table_name %>.description = params[:description] if params[:description]
    set_updater(@<%= singular_table_name %>)
    @<%= singular_table_name %>.save!
    api_render @<%= singular_table_name %>
  end


  # DELETE <%= route_url %>/a-b-c-d-e
  def destroy
    @<%= singular_table_name %>.destroy
    render_head_204
  end


  private

  def find_<%= singular_table_name %>
    ActionController::Parameters.permit_all_parameters = true
    @<%= singular_table_name %> = <%= class_name %>.find_by_key(params['id'], consistent: true)
    return true if @<%= singular_table_name %>
    render_api_error 404, "<%= class_name %> not found"
    false
  end

end
<% end -%>
