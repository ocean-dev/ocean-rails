require 'spec_helper'

describe <%= class_name.pluralize %>Controller do
  
  render_views
  
  describe "POST" do
    
    before :each do
      <%= class_name %>.delete_all
      permit_with 200
      request.headers['HTTP_ACCEPT'] = "application/json"
      request.headers['X-API-Token'] = "incredibly-fake!"
      @args = build(:<%= singular_name %>).attributes
    end
    
    
    it "should return JSON" do
      post :create, @args
      expect(response.content_type).to eq "application/json"
    end
    
    it "should return a 400 if the X-API-Token header is missing" do
      request.headers['X-API-Token'] = nil
      post :create, @args
      expect(response.status).to eq 400
    end
    
    it "should return a 201 when successful" do
      post :create, @args
      expect(response).to render_template(partial: "_<%= singular_name %>", count: 1)
      expect(response.status).to eq 201
    end

    it "should contain a Location header when successful" do
      post :create, @args
      expect(response.headers['Location']).to be_a String
    end

    it "should return the new resource in the body when successful" do
      post :create, @args
      expect(response.body).to be_a String
    end
    
    #
    # Uncomment this test as soon as there is one or more DB attributes that define
    # the uniqueness of a record.
    #
    # it "should return a 422 if the <%= singular_name %> already exists" do
    #   post :create, @args
    #   expect(response.status).to eq 201
    #   expect(response.content_type).to eq "application/json"
    #   post :create, @args
    #   expect(response.status).to eq 422
    #   expect(response.content_type).to eq "application/json"
    #   expect(JSON.parse(response.body).to eq({"_api_error" => ["<%= class_name %> already exists"]})
    # end

    #
    # Uncomment this test as soon as there is one or more DB attributes that need
    # validating.
    #
    # it "should return a 422 when there are validation errors" do
    #   post :create, @args.merge('name' => "qz")
    #   expect(response.status).to eq 422
    #   expect(response.content_type).to eq "application/json"
    #   expect(JSON.parse(response.body)).to eq({"name"=>["is too short (minimum is 3 characters)"]})
    # end
                
  end
  
end
