require 'spec_helper'

describe <%= class_name %> do


  describe "attributes" do
    
    it "should have a name" do
      expect(create(:<%= singular_name %>).name).to be_a String
    end

    it "should have a description" do
      expect(create(:<%= singular_name %>).description).to be_a String
    end

     it "should have a creation time" do
      expect(create(:<%= singular_name %>).created_at).to be_a Time
    end

    it "should have an update time" do
      expect(create(:<%= singular_name %>).updated_at).to be_a Time
    end
  
   it "should have a creator" do
      expect(create(:<%= singular_name %>).created_by).to be_a String
    end

    it "should have an updater" do
      expect(create(:<%= singular_name %>).updated_by).to be_a String
    end

  end


  describe "relations" do

  end

end
