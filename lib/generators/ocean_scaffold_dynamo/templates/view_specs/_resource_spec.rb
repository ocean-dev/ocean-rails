require 'spec_helper'

describe "<%= plural_name %>/_<%= singular_name %>" do
  
  before :each do                     # Must be :each (:all causes all tests to fail)
    <%= class_name %>.delete_all
    render partial: "<%= plural_name %>/<%= singular_name %>", locals: {<%= singular_name %>: create(:<%= singular_name %>)}
    @json = JSON.parse(rendered)
    @u = @json['<%= singular_name %>']
    @links = @u['_links'] rescue {}
  end


  it "has a named root" do
    expect(@u).not_to eq nil
  end


  it "should have three hyperlinks" do
    expect(@links.size).to eq 3
  end

  it "should have a self hyperlink" do
    expect(@links).to be_hyperlinked('self', /<%= plural_name %>/)
  end

  it "should have a creator hyperlink" do
    expect(@links).to be_hyperlinked('creator', /api_users/)
  end

  it "should have an updater hyperlink" do
    expect(@links).to be_hyperlinked('updater', /api_users/)
  end


  it "should have a name" do
    expect(@u['name']).to be_a String
  end

  it "should have a description" do
    expect(@u['description']).to be_a String
  end

  it "should have a created_at time" do
    expect(@u['created_at']).to be_a String
  end

  it "should have an updated_at time" do
    expect(@u['updated_at']).to be_a String
  end

  it "should have a lock_version field" do
    expect(@u['lock_version']).to be_an Integer
  end
      
end
