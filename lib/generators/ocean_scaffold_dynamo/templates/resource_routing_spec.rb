require "spec_helper"

describe <%= class_name.pluralize %>Controller do
  describe "routing" do

    it "routes to #index" do
      expect(get("/v1/<%= plural_name %>")).to route_to("<%= plural_name %>#index")
    end

    it "routes to #show" do
      expect(get("/v1/<%= plural_name %>/1-2-3-4-5")).to route_to("<%= plural_name %>#show", id: "1-2-3-4-5")
    end

    it "routes to #create" do
      expect(post("/v1/<%= plural_name %>")).to route_to("<%= plural_name %>#create")
    end

    it "routes to #update" do
      expect(put("/v1/<%= plural_name %>/1-2-3-4-5")).to route_to("<%= plural_name %>#update", id: "1-2-3-4-5")
    end

    it "routes to #destroy" do
      expect(delete("/v1/<%= plural_name %>/1-2-3-4-5")).to route_to("<%= plural_name %>#destroy", id: "1-2-3-4-5")
    end

  end
end
