require 'spec_helper'

describe <%= class_name %> do


  describe "attributes" do
    
    it "should have a name" do
      expect(create(:<%= singular_name %>).name).to be_a String
    end

    it "should have a description" do
      expect(create(:<%= singular_name %>).description).to be_a String
    end

     it "should have a creation time" do
      expect(create(:<%= singular_name %>).created_at).to be_a Time
    end

    it "should have an update time" do
      expect(create(:<%= singular_name %>).updated_at).to be_a Time
    end
  
   it "should have a creator" do
      expect(create(:<%= singular_name %>).created_by).to be_a String
    end

    it "should have an updater" do
      expect(create(:<%= singular_name %>).updated_by).to be_a String
    end

  end


  describe "relations" do

  end



  describe "search" do
  
    describe ".collection" do
    
      before :each do
        create :<%= singular_name %>, name: 'foo', description: "The Foo object"
        create :<%= singular_name %>, name: 'bar', description: "The Bar object"
        create :<%= singular_name %>, name: 'baz', description: "The Baz object"
      end
      
    
      it "should return an array of <%= class_name %> instances" do
        ix = <%= class_name %>.collection
        expect(ix.length).to eq 3
        expect(ix[0]).to be_a <%= class_name %>
      end
    
      it "should allow matches on name" do
        expect(<%= class_name %>.collection(name: 'NOWAI').length).to eq 0
        expect(<%= class_name %>.collection(name: 'bar').length).to eq 1
        expect(<%= class_name %>.collection(name: 'baz').length).to eq 1
      end
      
      it "should allow searches on description" do
        expect(<%= class_name %>.collection(search: 'a').length).to eq 2
        expect(<%= class_name %>.collection(search: 'object').length).to eq 3
      end
      
      it "key/value pairs not in the index_only array should quietly be ignored" do
        expect(<%= class_name %>.collection(name: 'bar', aardvark: 12).length).to eq 1
      end
        
    end
  end

end
