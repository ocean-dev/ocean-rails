require 'spec_helper'

describe <%= class_name.pluralize %>Controller do
  
  render_views

  describe "DELETE" do
    
    before :each do
      <%= class_name %>.delete_all
      permit_with 200
      @<%= singular_name %> = create :<%= singular_name %>
      request.headers['HTTP_ACCEPT'] = "application/json"
      request.headers['X-API-Token'] = "so-totally-fake"
    end

    
    it "should return JSON" do
      delete :destroy, id: @<%= singular_name %>
      expect(response.content_type).to eq "application/json"
    end

    it "should return a 400 if the X-API-Token header is missing" do
      request.headers['X-API-Token'] = nil
      delete :destroy, id: @<%= singular_name %>
      expect(response.status).to eq 400
    end
    
    it "should return a 204 when successful" do
      delete :destroy, id: @<%= singular_name %>
      expect(response.status).to eq 204
      expect(response.content_type).to eq "application/json"
    end

    it "should return a 404 when the <%= class_name %> can't be found" do
      delete :destroy, id: "0-0-0-0-0"
      expect(response.status).to eq 404
    end
    
    it "should destroy the <%= class_name %> when successful" do
      delete :destroy, id: @<%= singular_name %>
      expect(response.status).to eq 204
      expect(<%= class_name %>.find_by_id(@<%= singular_name %>.id)).to be_nil
    end
    
  end
  
end
