require "ocean/engine"

require "ocean-dynamo"
require "ocean/api"
require "ocean/api_resource"
require "ocean/ocean_resource_model" if defined? ActiveRecord || defined? OceanDynamo
require "ocean/ocean_resource_controller" if defined? ActionController
require "ocean/ocean_application_controller"

#if ENV['RAILS_ENV'] == "production"
#  require "ocean/selective_rack_logger"
#end

require "ocean/api_remote_resource"


INVALIDATE_MEMBER_DEFAULT =     ["($|/|\\?)"]
INVALIDATE_COLLECTION_DEFAULT = ["($|\\?)"]

UUID_REGEX = /([0-9a-f]+-){4}[0-9a-f]+/


module Ocean

  if ENV['RAILS_ENV'] == "production"
    class Railtie < Rails::Railtie
      # Silence the /alive action
      #initializer "ocean.swap_logging_middleware" do |app|
      #  app.middleware.swap Rails::Rack::Logger, SelectiveRackLogger
      #end
      # Make sure the generators use the gem's templates first
      config.app_generators do |g|
        g.templates.unshift File::expand_path('../templates', __FILE__)
      end
    end
  end

end


#
# For stubbing successful authorisation calls. Makes <tt>Api.permitted?</tt> return
# the status, and a body containing a partial authentication containing the +user_id+
# and +creator_uri+ given by the parameters. It also allows the value of 'right' to
# be specified: this will restrict all SQL queries accordingly.
#
def permit_with(status, user_id: "1-2-3-4-5", creator_uri: "https://api.example.com/v1/api_users/#{user_id}",
                        right: nil, group_names: [])
  allow(Api).to receive(:permitted?).
    and_return(double(:status => status,
                      :body => {'authentication' =>
                                 {'right' => right,
                                  'group_names' => group_names,
                                  '_links' => { 'creator' => {'href' => creator_uri,
                                                              'type' => 'application/json'}}}}))
end


#
# For stubbing failed authorisation calls. Makes <tt>Api.permitted?</tt> return the
# given status and a body containing a standard API error with the given error messages.
#
def deny_with(status, *error_messages)
  allow(Api).to receive(:permitted?).
    and_return(double(:status => status,
                      :body => {'_api_error' => error_messages}))
end


#
# Takes a relation and adds right restrictions, if present.
#
def add_right_restrictions(rel, restrictions)
  return rel unless restrictions
  # First get the table to use as a basis for the OR clauses
  t = rel.arel_table
  # Accumulating Arel AND clauses
  cond = restrictions.reduce [] do |acc, rr|
    app = rr['app']
    context = rr['context']
    if    app != '*' && context != '*'
      acc << t[:app].eq(app).and(t[:context].eq(context))
    elsif app != '*' && context == '*'
      acc << t[:app].eq(app)
    elsif app == '*' && context != '*'
      acc << t[:context].eq(context)
    end
    acc
  end
  # Process the clauses. We might not need to OR anything.
  case cond.length
  when 0
    return rel
  when 1
    return rel.where(cond.first)
  else
    # OR the multiple clauses together
    cond = cond.reduce :or
    # Return a relation built from the Arel condition we've constructed
    rel.where(cond)
  end
end


#
# Used in Jbuilder templates to build hyperlinks
#
def hyperlinks(links={})
  result = {}
  links.each do |qi, val|
    next unless val.present?
    result[qi.to_s] = {
               "href" => val.kind_of?(String) ? val : val[:href],
               "type" => val.kind_of?(String) ? "application/json" : val[:type]
            }
  end
  result
end

#
# This renders creator and updater links correctly.
#
def smart_api_user_url(x)
  if x.blank? || x == "0" || x == 0
    false #"#{OCEAN_API_URL}/#{Api.version_for :api_user}/api_users/0-0-0-0-0"
  elsif x.is_a?(Integer)
    "#{OCEAN_API_URL}/#{Api.version_for :api_user}/api_users/#{x}"
  elsif x.is_a?(String)
    x =~ /^http(s)?:\/\// ? x : "#{OCEAN_API_URL}/#{Api.version_for :api_user}/api_users/#{x}"
  else
    raise "api_user_url takes an integer, a string, or nil"
  end
end


#
# View helper predicates to determine if the ApiUser behind the current
# authorisation belongs to one or more of a list of Groups.
#
def member_of_group?(*names)
  @group_names && @group_names.intersect?(names.to_set)
end

#
# Returns true if the ApiUser behind the current authorisation belongs
# to the Ocean Group "Superusers".
#
def superuser?
  member_of_group?("Superusers")
end


def aws_config(endpoint: nil, **keywords)
  if ENV['AWS_ACCESS_KEY_ID'].present? && ENV['AWS_SECRET_ACCESS_KEY'].present?
    raise "AWS_REGION not defined" unless ENV['AWS_REGION'].present?
    {
      region: ENV['AWS_REGION'],
      credentials: Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'],
                                        ENV['AWS_SECRET_ACCESS_KEY'])
    }
  elsif endpoint.present? && ENV[endpoint].present?
    {
      region: "eu-west-1",    # Doesn't matter in this context
      credentials: Aws::Credentials.new('not', 'used'),
      endpoint: ENV[endpoint]
    }.merge(keywords)
  else
    raise "AWS_REGION not defined" unless ENV['AWS_REGION'].present?
    credential_handler = ENV['AWS_CONFIG_LOCAL_MODE'].present? ? Aws::InstanceProfileCredentials :
                                                                 Aws::ECSCredentials
    {
      region: ENV['AWS_REGION'],
      credentials: credential_handler.new
    }
  end
end
